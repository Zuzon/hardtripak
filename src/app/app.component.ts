/**
 * Angular 2 decorators and services
 */
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { AppState } from './app.service';
import { MatSnackBar } from '@angular/material';

export const ROOT_SELECTOR = 'app';

/**
 * App Component
 * Top Level Component
 */
@Component({
  selector: ROOT_SELECTOR,
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css'
  ],
  templateUrl: './tpl.html'
})
export class AppComponent implements OnInit {
  public name = 'Angular Starter';
  public tipe = 'assets/img/tipe.png';
  public twitter = 'https://twitter.com/gdi2290';
  public url = 'https://tipe.io';
  public showDevModule: boolean = environment.showDevModule;

  constructor(
    public appState: AppState,
    public router: Router,
    public snackBar: MatSnackBar,
    public http: HttpClient
  ) {}

  public ngOnInit() {
    console.log('Initial App State', this.appState.state);
    this.appState.passengerRequest.subscribe(() => {
      this.openSnackBar();
    });
    this.appState.onNotification.subscribe((data) => {
      this.snackBar.open(data.user.username + ' ' + data.confirmation+'d your request', null, {
          duration: 2000
      });
    });
  }

  public logout() {
    localStorage.removeItem('userId');
    this.router.navigate(['/']);
  }

  public respondPassenger(tripId, userId, status) {
    console.log('respond to passenger', tripId, userId, status);
    this.http.get('http://kvit.lv/ct/api/confirm', {
      params: {
        user_id: localStorage.getItem('userId'),
        trip_id: tripId,
        second_id: userId,
        status
      }
    }).subscribe((response) => {
      console.log('responded to passenger', response);
    });
  }

  openSnackBar() {
      this.snackBar.open('New passenger wants to join!!!', null, {
          duration: 2000
      });
  }

}

/**
 * Please review the https://github.com/AngularClass/angular-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
