import {
  Injectable,
  EventEmitter
} from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface InternalStateType {
  [key: string]: any;
}

@Injectable()
export class AppState {

  public passengerRequest: EventEmitter<void> = new EventEmitter();
  public onNotification: EventEmitter<any> = new EventEmitter();
  public passengerReqests: any[] = [];

  constructor(public http: HttpClient) {
    setInterval(() => {
      this.pollPassengerRequests();
      this.getNotifications();
    }, 2000);
  }

  public getNotifications() {
    this.http.get('http://kvit.lv/ct/api/notifications', {
      params: {
        user_id: localStorage.getItem('userId')
      }
    }).subscribe((result: any) => {
      console.log('notifications', result);
      if (result.length) {
        this.http.get('http://kvit.lv/ct/api/notified?user_id=2&trip_id=5', {
          params: {
            user_id: localStorage.getItem('userId'),
            trip_id: result[0].trip_id
          }
        }).subscribe((fin) => {
          this.onNotification.emit(result[0]);
        });
      }
    });
  }

  public pollPassengerRequests() {
    this.http.get('http://kvit.lv/ct/api/user_trips', {
      params: {
        user_id: localStorage.getItem('userId')
      }
    }).subscribe((result: any) => {
      console.log('passenger requests', result);
      let actualRequests = [];
      for (let item of result) {
        if (!item.confirmation) {
          actualRequests.push(item);
        }
      }
      if (this.passengerReqests.length < actualRequests.length) {
        this.passengerRequest.emit();
      }
      this.passengerReqests = actualRequests;
    });
  }

  public _state: InternalStateType = { };

  /**
   * Already return a clone of the current state.
   */
  public get state() {
    return this._state = this._clone(this._state);
  }
  /**
   * Never allow mutation
   */
  public set state(value) {
    throw new Error('do not mutate the `.state` directly');
  }

  public get(prop?: any) {
    /**
     * Use our state getter for the clone.
     */
    const state = this.state;
    return state.hasOwnProperty(prop) ? state[prop] : state;
  }

  public set(prop: string, value: any) {
    /**
     * Internally mutate our state.
     */
    return this._state[prop] = value;
  }

  private _clone(object: InternalStateType) {
    /**
     * Simple object clone.
     */
    return JSON.parse(JSON.stringify( object ));
  }
}
