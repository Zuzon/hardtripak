import {
    Component,
    OnInit,
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'login',
    styleUrls: [],
    templateUrl: './tpl.html'
})
export class LoginComponent {
    public username: string = '';
    constructor(
        public router: Router,
        public http: HttpClient
    ) {
        let userId = localStorage.getItem('userId');
        if (userId) {
            router.navigate(['/home']);
        }
    }

    public login() {
        // login
        this.http.get('http://kvit.lv/ct/api/user', { params: {
            username: this.username
        }}).subscribe((result: any) => {
            console.log('login', result);
            localStorage.setItem('userId', result.id.toString());
            localStorage.setItem('username', result.username);
            localStorage.setItem('type', '');
            this.router.navigate(['/home']);
        });
    }
}