import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { ChooseComponent } from './scenario-choose/component';
import { SearchComponent } from './passenger/search/component';
import { TripsComponent } from './passenger/trips/component';
import { CreateTripComponent } from './driver/create-trip/component';
import { MyTripsComponent } from './my-trips/component';
import { LoginComponent } from './login/component';

export const ROUTES: Routes = [
  { path: '',      component: LoginComponent },
  { path: 'home',  component: ChooseComponent },
  { path: 'about', component: AboutComponent },
  { path: 'search', component: SearchComponent },
  { path: 'trips', component: TripsComponent},
  { path: 'create-trip', component: CreateTripComponent},
  { path: 'my-trips', component: MyTripsComponent},
  { path: '**',    component: NoContentComponent },
];
