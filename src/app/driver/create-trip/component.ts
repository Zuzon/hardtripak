import {
    Component,
    OnInit
} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'create',
    styleUrls: [],
    templateUrl: './tpl.html'
})
export class CreateTripComponent {
    myControl = new FormControl();
    options: string[] = ['Riga', 'Daugavpils', 'Lociki'];
    min: string[] = ['00', '15', '30', '45'];
    hour: string[] = [];

    public from: string;
    public to: string;
    public date: Date;
    public timeH: string;
    public timeM: string;
    constructor(
        public router: Router,
        public http: HttpClient
    ) {
        for (let i = 0; i < 24; i++)
        {
            if (i < 10) {
                this.hour.push('0'+i.toString());
            } else {
                this.hour.push(i.toString());
            }
        }
        this.http.get('http://kvit.lv/ct/api/user_type', { params: {
            user_id: localStorage.getItem('userId'),
            type: 'driver'
        }}).subscribe((result) => {
            console.log('set user type driver', result);
            localStorage.setItem('userType', 'driver');
        });
     }

     public createTrip(){
         console.log('create trip', this.from, this.to, this.date, this.timeH, this.timeM);
         this.http.get('http://kvit.lv/ct/api/create_trip', { params: {
            user_id: localStorage.getItem('userId'),
            from_city: this.from,
            to_city: this.to,
            date: this.date.toLocaleDateString("en-US"),
            time: this.timeH + ':' + this.timeM
        }}).subscribe((result) => {
            console.log('create trip result', result);
            this.router.navigate(['/my-trips']);
        });
     }
}