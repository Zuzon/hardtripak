import {
    Component,
    OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'search',
    styleUrls: [],
    templateUrl: './tpl.html'
})
export class SearchComponent {
    myControl = new FormControl();
    options: string[] = ['Riga', 'Daugavpils', 'Lociki'];
    constructor(
        public route: ActivatedRoute
    ) { }
}