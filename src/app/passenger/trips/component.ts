import {
    Component,
    OnInit,
    ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'trips',
    styleUrls: ['./styles.scss'],
    templateUrl: './tpl.html'
})
export class TripsComponent implements OnInit {
    constructor(
        public router: Router,
        public snackBar: MatSnackBar,
        public http: HttpClient,
        private cdref: ChangeDetectorRef
    ) { }
    displayedColumns: string[] = ['date', 'route', 'driver', 'actions'];
    dataSource: any[];
    openSnackBar(message: string) {
        this.snackBar.open(message, null, {
            duration: 2000,
        });
    }

    public ngOnInit() {
        this.http.get('http://kvit.lv/ct/api/trips', {
            params: {
                user_id: localStorage.getItem('userId')
            }
        }).subscribe((result: any[]) => {
            console.log('trip list', result);
            for (let item of result) {
                item.date = new Date(item.date);
            }
            this.dataSource = result;
            this.cdref.markForCheck();
        });
    }

    public selectTrip(id) {
        this.http.get('http://kvit.lv/ct/api/choose_trip', {
            params: {
                user_id: localStorage.getItem('userId'),
                trip_id: id
            }
        }).subscribe(() => {
            this.openSnackBar('Driver will be notified! Keep in touch!');
            this.router.navigate(['/home']);
        });
    }
}

export interface Trip {
    date: Date;
    time: string;
    name: string;
    image: string;
    rating: number;
    totalPlaces: number;
    placesLeft: number;
    isRegistered: boolean;
}

const ELEMENT_DATA: Trip[] = [
    {
        date: new Date(),
        time: '18:00',
        name: 'Vasja',
        image: '',
        rating: 0.3,
        totalPlaces: 5,
        placesLeft: 3,
        isRegistered: true
    },
    {
        date: new Date(),
        time: '19:00',
        name: 'Petja',
        image: '',
        rating: 0.3,
        totalPlaces: 2,
        placesLeft: 1,
        isRegistered: false
    }
];