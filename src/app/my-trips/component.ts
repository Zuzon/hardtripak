import {
    Component,
    OnInit,
    ChangeDetectorRef
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'my-trips',
    styleUrls: ['./styles.scss'],
    templateUrl: './tpl.html'
})
export class MyTripsComponent implements OnInit {
    dataSource: any[];
    displayedColumns: string[] = ['date', 'route', 'status', 'actions'];
    constructor(
        public route: ActivatedRoute,
        public http: HttpClient,
        private cdref: ChangeDetectorRef
    ) { }

    public ngOnInit() {
        this.http.get('http://kvit.lv/ct/api/trips', {
            params: {
                user_id: localStorage.getItem('userId')
            }
        }).subscribe((result: any[]) => {
            console.log('my trip list', result);
            let newArr = [];
            for (let item of result) {
                item.date = new Date(item.date);
                if (item.user_id === parseInt(localStorage.getItem('userId'), 10)) {
                    newArr.push(item);
                }
            }
            this.dataSource = newArr;
            this.cdref.markForCheck();
        });
    }
}